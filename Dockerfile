FROM ruby:2.7.1
RUN mkdir /gitlab-k8s
COPY config.ru Gemfile Gemfile.lock /gitlab-k8s/
WORKDIR /gitlab-k8s
RUN bundle install --verbose
EXPOSE 9292
CMD ["/usr/local/bin/bundle", "exec", "rackup", "config.ru", "-o", "0.0.0.0"]
